import {createStore, applyMiddleware} from 'redux';
import rootReducer from '../reducers';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';

export default function configureStore(initialState) {

  // NOTE: We can add more middleware:
  // - add support for store hot reload
  // - add support for redux dev tools extensions for Chrome
  // -> Check https://github.com/coryhouse/react-slingshot for more examples.

  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(reduxImmutableStateInvariant())
  );
}
