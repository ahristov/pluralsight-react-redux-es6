import * as types from '../actions/actionTypes';

// NOTE: We define default value - empty arrays of courses
export default function courseReducer(state = [], action) {
  switch(action.type) {
    case types.CREATE_COURSE:
      return [...state,
        Object.assign({}, action.course)
      ];

    default:
      return state;
  }
}
