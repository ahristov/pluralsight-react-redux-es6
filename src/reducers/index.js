// Contains the root reducer
import {combineReducers} from 'redux';
import courses from './courseReducer';

  // NOTE: The file is named `courseReducer`
  // but we alias it to `courses`, and so add it to the store

const rootReducer = combineReducers({
  courses
});

export default rootReducer;
