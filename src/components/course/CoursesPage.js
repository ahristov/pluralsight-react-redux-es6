import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as courseActions from '../../actions/courseActions';

class CoursesPage extends React.Component {

  // Initialize state for the form in the constructor
  constructor(props, context) {
    super(props, context);

    // Initialize state
    this.state = {
      course: { title: "" }
    };

    // Bind the context to the instance of the class
    this.onTitleChange = this.onTitleChange.bind(this);
    this.onClickSave = this.onClickSave.bind(this);

    // NOTE: We can use bind() inside render(), but it has performance issues,
    // as will execute with every call to render().
    // On each render() new function will be created.
    //  onChange={this.onTitleChange.bind(this)}

  }

  onTitleChange(event) {
    const course = this.state.course;
    course.title = event.target.value;
    this.setState({ course: course });
  }

  onClickSave() {
    // this.props.dispatch(courseActions.createCourse(this.state.course));
    // this.props.createCourse(this.state.course);
    this.props.actions.createCourse(this.state.course);
  }

  courseRow(course, index) {
    return <div key={index}>{course.title}</div>;
  }

  // NOTE: The form normally will be into a separate component that contains the markup
  render() {
    return (
      <div>
        <h1>Courses</h1>
        {this.props.courses.map(this.courseRow)}
        <h2>Add Course</h2>
        <input
          type="text"
          onChange={this.onTitleChange}
          value={this.state.course.title} />
        <input
          type="submit"
          value="Save"
          onClick={this.onClickSave} />
      </div>
    );
  }
}

CoursesPage.propTypes = {
  // Only needed when omitting the `mapDispatchToProps()`
  // dispatch: PropTypes.func.isRequired,
  // createCourse: PropTypes.func.isRequired,
  courses: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
  return {
    courses: state.courses
  };
}

function mapDispatchToProps(dispatch) {
  return {
    // createCourse: course => dispatch(courseActions.createCourse(course))

    // NOTE: wrap all the actions from courseActions
    actions: bindActionCreators(courseActions, dispatch)
  };
}


// NOTE: If we omit mapDispatchToProps parameter,
// the component gets a dispatch attached to it.
//  export default connect(mapStateToProps)(CoursesPage);
//
// Then this is how we use it:
//  onClickSave() {
//    this.props.dispatch(courseActions.createCourse(this.state.course));
//  }


export default connect(mapStateToProps, mapDispatchToProps)(CoursesPage);
// export default connect(mapStateToProps)(CoursesPage);
